# SGT : Swift Network Scanner

Ce programme en JavaScript utilise la bibliothèque `node-libnmap` pour effectuer un scan de réseaux et afficher les informations des hôtes scannés.

## Prérequis

Avant d'exécuter ce programme, assurez-vous d'avoir installé Node.js sur votre machine.

## Installation

1. Clonez ce dépôt vers votre machine :

`git clone https://github.com/votre-utilisateur/network-scanner.git`


2. Accédez au répertoire du projet :

`cd network-scanner`


3. Installez les dépendances du projet :

`npm install`


## Utilisation

1. Ouvrez le fichier `index.js` et personnalisez les options de scan dans la variable `options` en fonction de vos besoins.

2. Exécutez le programme avec la commande suivante :

`node index.js`


Le programme effectuera le scan du réseau en utilisant les options spécifiées et affichera les informations des hôtes scannés.

## Personnalisation

- Vous pouvez modifier les options de scan (plage d'adresses IP, ports à scanner, etc.) dans le fichier `index.js` selon vos besoins.

- Vous pouvez également personnaliser le format et le contenu des informations affichées dans la boucle de parcours des résultats du scan.

## Contributions

Les contributions sont les bienvenues ! Si vous souhaitez améliorer ce programme ou ajouter de nouvelles fonctionnalités, n'hésitez pas à ouvrir une issue ou à soumettre une pull request.

## Licence

Ce projet est sous licence [GNU].
