const nmap = require('libnmap');

const options = {
  range: ['192.168.0.1-100'], // Plage d'adresses IP à scanner
  ports: '80,443', // Ports à scanner
};

nmap.scan(options, (err, report) => {
  if (err) {
    console.error(err);
    return;
  }

  const { scan } = report;

  // Parcourir les résultats du scan
  scan.forEach((host) => {
    console.log(`Adresse IP : ${host.ip}`);
    console.log(`État : ${host.status.state}`);
    console.log(`Ports ouverts : ${host.ports.length}`);

    // Parcourir les ports ouverts
    host.ports.forEach((port) => {
      console.log(`- Port : ${port.port}`);
      console.log(`  Protocole : ${port.protocol}`);
      console.log(`  État : ${port.state}`);
    });

    console.log('---');
  });
});

